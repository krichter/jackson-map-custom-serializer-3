package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class TheTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(TheTest.class);

    @Test
    public void testSimplestJsonDeserialization() throws JsonProcessingException, IOException {
        Entity1 offer = new Entity1();
        ObjectMapper mapper = new ObjectMapper();
        String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(offer);
        LOGGER.trace(String.format("serialzed: %s",
                serialized));
        Entity1 deserialized = mapper.readValue(serialized, new TypeReference<Entity1>() {});
        assertEquals(offer,
                deserialized);
    }

    @Test
    public void testJsonDeserialization() throws JsonProcessingException, IOException {
        Entity3 offerProperty = new Entity3("name");
        long offerPropertyId = 2l;
        offerProperty.setId(offerPropertyId);
        Entity2 offerType = new Entity2(new HashSet<>(Arrays.asList(offerProperty)));
        long offerTypeId = 3l;
        offerType.setId(offerTypeId);
        Entity1 offer = new Entity1(offerType,
                new ImmutableMap.Builder<Entity3, String>().put(offerProperty, "0").build());
        long offerId = 4l;
        offer.setId(offerId);

        ObjectMapper mapper = new ObjectMapper();
        String serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(offer);
        LOGGER.trace(String.format("serialzed: %s",
                serialized));
        Entity1 deserialized = mapper.readValue(serialized, new TypeReference<Entity1>() {});
        assertEquals(offer,
                deserialized);
    }
}
