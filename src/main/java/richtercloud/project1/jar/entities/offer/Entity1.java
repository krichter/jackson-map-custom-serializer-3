package richtercloud.project1.jar.entities.offer;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Entity1 extends AbstractEntity1 {
    private static final long serialVersionUID = 1L;
    private Entity1 original = null;
    private Entity1Edit pendingEdit = null;
    private List<Entity1Edit> edits = new LinkedList<>();

    public Entity1() {
    }

    public Entity1(Entity2 offerType,
            Map<Entity3, String> valueMap) {
        super(offerType,
                valueMap);
    }

    public Entity1Edit getPendingEdit() {
        return pendingEdit;
    }

    public void setPendingEdit(Entity1Edit pendingEdit) {
        this.pendingEdit = pendingEdit;
    }

    public Entity1 getOriginal() {
        return original;
    }

    public void setOriginal(Entity1 original) {
        this.original = original;
    }

    @XmlTransient
    public List<Entity1Edit> getEdits() {
        return edits;
    }

    public void setEdits(List<Entity1Edit> edits) {
        this.edits = edits;
    }
}
