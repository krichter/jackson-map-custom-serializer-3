package richtercloud.project1.jar.entities.offer;

import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Entity1Edit extends AbstractEntity1 {
    private static final long serialVersionUID = 1L;
    private Entity1 ofOffer;

    public Entity1Edit() {
    }

    public Entity1Edit(Entity1 ofOffer,
            Entity2 offerType,
            Map<Entity3, String> valueMap) {
        super(offerType,
                valueMap);
        this.ofOffer = ofOffer;
    }

    public Entity1 getOfOffer() {
        return ofOffer;
    }

    public void setOfOffer(Entity1 ofOffer) {
        this.ofOffer = ofOffer;
    }
}
