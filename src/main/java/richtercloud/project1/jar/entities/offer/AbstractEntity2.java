package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;
import richtercloud.project1.jar.entities.Identifiable;

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id",
    scope=AbstractEntity2.class)
@XmlRootElement
public abstract class AbstractEntity2 extends Identifiable {
    private static final long serialVersionUID = 1L;
    private Set<Entity3> properties = new HashSet<>();
    @JsonSerialize(using = Entity2MandatorySerializer.class)
    @JsonDeserialize(using = Entity2MandatoryDeserializer.class)
    private Map<Entity3, Boolean> mandatory = new HashMap<>();
    @JsonSerialize(using = Entity2RecommendedSerializer.class)
    @JsonDeserialize(using = Entity2RecommendedDeserializer.class)
    private Map<Entity3, Boolean> recommended = new HashMap<>();

    public AbstractEntity2() {
    }

    public AbstractEntity2(Set<Entity3> properties) {
        this.properties = properties;
    }

    public Set<Entity3> getProperties() {
        return properties;
    }

    public void setProperties(Set<Entity3> properties) {
        this.properties = properties;
    }

    public Map<Entity3, Boolean> getMandatory() {
        return mandatory;
    }

    public void setMandatory(Map<Entity3, Boolean> mandatory) {
        this.mandatory = mandatory;
    }

    public Map<Entity3, Boolean> getRecommended() {
        return recommended;
    }

    public void setRecommended(Map<Entity3, Boolean> recommended) {
        this.recommended = recommended;
    }

//    @Override
//    public int hashCode() {
//        int hash = 7;
//        hash = 89 * hash + Objects.hashCode(this.properties);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final AbstractEntity2 other = (AbstractEntity2) obj;
//        if (!Objects.equals(this.properties, other.properties)) {
//            return false;
//        }
//        return true;
//    }
}
