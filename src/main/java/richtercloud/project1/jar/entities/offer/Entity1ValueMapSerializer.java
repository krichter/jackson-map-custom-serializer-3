package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.databind.type.TypeFactory;
import java.util.Map;

/**
 *
 * @author richter
 */
public class Entity1ValueMapSerializer extends MapEntrySerializer<Entity3, String> {
    private static final long serialVersionUID = 1L;

    public Entity1ValueMapSerializer() {
        super(TypeFactory.defaultInstance().constructMapType(Map.class, Entity3.class, String.class));
    }
}
