package richtercloud.project1.jar.entities.offer;

import java.util.Date;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Entity2Edit extends AbstractEntity2 {
    private static final long serialVersionUID = 1L;
    private Entity2 ofOfferType;

    public Entity2Edit() {
    }

    public Entity2Edit(Entity2 ofOfferType,
            Set<Entity3> properties) {
        super(properties);
        this.ofOfferType = ofOfferType;
    }

    public Entity2 getOfOfferType() {
        return ofOfferType;
    }

    public void setOfOfferType(Entity2 ofOfferType) {
        this.ofOfferType = ofOfferType;
    }
}
