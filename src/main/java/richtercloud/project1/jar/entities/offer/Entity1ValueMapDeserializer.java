package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author richter
 */
public class Entity1ValueMapDeserializer extends StdDeserializer<Map<Entity3, String>> {
    //do not extend MapEntryDeserializer<OfferProperty, Integer>, but copy the
    //code, see MapEntryDeserializer for an explanation
    private static final long serialVersionUID = 1L;

    public Entity1ValueMapDeserializer() {
        super(TypeFactory.defaultInstance().constructMapType(Map.class, Entity3.class, String.class));
    }

    @Override
    public Map<Entity3, String> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Map<Entity3, String> retValue = new HashMap<>();
        List<Map.Entry<Entity3, String>> entries = p.readValueAs(new TypeReference<List<Map.Entry<Entity3, String>>>() {});
        for(Map.Entry<Entity3, String> entry : entries) {
            retValue.put(entry.getKey(),
                    entry.getValue());
        }
        return retValue;
    }
}
