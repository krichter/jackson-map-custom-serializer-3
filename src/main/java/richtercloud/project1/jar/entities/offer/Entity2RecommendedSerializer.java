package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.databind.type.TypeFactory;
import java.util.Map;

/**
 *
 * @author richter
 */
public class Entity2RecommendedSerializer extends MapEntrySerializer<Entity3, Boolean> {
    private static final long serialVersionUID = 1L;

    public Entity2RecommendedSerializer() {
        super(TypeFactory.defaultInstance().constructMapType(Map.class, Entity3.class, Boolean.class));
    }
}
