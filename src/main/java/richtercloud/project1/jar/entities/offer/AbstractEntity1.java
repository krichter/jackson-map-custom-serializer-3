package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;
import java.util.Map;
import richtercloud.project1.jar.entities.Identifiable;
import java.util.Objects;

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id",
    scope=AbstractEntity1.class)
public abstract class AbstractEntity1 extends Identifiable {
    private static final long serialVersionUID = 1L;
    private Entity2 offerType;
    @JsonSerialize(using = Entity1ValueMapSerializer.class)
    @JsonDeserialize(using = Entity1ValueMapDeserializer.class)
    private Map<Entity3, String> valueMap = new HashMap<>();

    public AbstractEntity1() {
    }

    public AbstractEntity1(Entity2 offerType,
            Map<Entity3, String> valueMap) {
        this.offerType = offerType;
        this.valueMap = valueMap;
    }

    /**
     * Copy constructor to create separate entities of type {@link OfferEdit} or
     * {@link OfferRevision}.
     *
     * @param offer the offer to copy
     */
    public AbstractEntity1(Entity1 offer) {
        this(offer.getOfferType(),
                new HashMap<>(offer.getValueMap()));
    }

    public Entity2 getOfferType() {
        return offerType;
    }

    public void setOfferType(Entity2 offerType) {
        this.offerType = offerType;
    }

    public Map<Entity3, String> getValueMap() {
        return valueMap;
    }

    public void setValueMap(Map<Entity3, String> valueMap) {
        this.valueMap = valueMap;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.valueMap);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractEntity1 other = (AbstractEntity1) obj;
        if (!Objects.equals(this.valueMap, other.valueMap)) {
            return false;
        }
        return true;
    }
}
