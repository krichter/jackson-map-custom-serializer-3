package richtercloud.project1.jar.entities.offer;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public abstract class Entity3Edit extends AbstractEntity3 {
    private static final long serialVersionUID = 1L;
    private Entity3 ofOfferProperty;

    public Entity3Edit() {
    }

    public Entity3Edit(Entity3 ofOfferProperty) {
        this.ofOfferProperty = ofOfferProperty;
    }

    public Entity3 getOfOfferProperty() {
        return ofOfferProperty;
    }

    public void setOfOfferProperty(Entity3 ofOfferProperty) {
        this.ofOfferProperty = ofOfferProperty;
    }
}
