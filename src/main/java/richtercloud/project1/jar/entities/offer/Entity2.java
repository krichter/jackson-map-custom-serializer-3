package richtercloud.project1.jar.entities.offer;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Entity2 extends AbstractEntity2 {
    private static final long serialVersionUID = 1L;
    private Entity2 original = null;
    private Entity2Edit pendingEdit = null;
    private List<Entity2Edit> edits = new LinkedList<>();

    public Entity2() {
    }

    public Entity2(Set<Entity3> properties) {
        super(properties);
    }

    public Entity2 getOriginal() {
        return original;
    }

    public void setOriginal(Entity2 original) {
        this.original = original;
    }

    public Entity2Edit getPendingEdit() {
        return pendingEdit;
    }

    public void setPendingEdit(Entity2Edit pendingEdit) {
        this.pendingEdit = pendingEdit;
    }

    @XmlTransient
    public List<Entity2Edit> getEdits() {
        return edits;
    }

    public void setEdits(List<Entity2Edit> edits) {
        this.edits = edits;
    }
}
