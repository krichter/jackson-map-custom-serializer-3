package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapEntryDeserializer<K,V> extends StdDeserializer<Map<K,V>> {
    private static final long serialVersionUID = 1L;

    public MapEntryDeserializer(Class<?> vc) {
        super(vc);
    }

    public MapEntryDeserializer(JavaType valueType) {
        super(valueType);
    }

    @Override
    public Map<K, V> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Map<K,V> retValue = new HashMap<>();
        List<Entry<K,V>> entries = p.readValueAs(new TypeReference<List<Entry<K,V>>>() {});
            //@TODO: this resolves to List<Entry<Object, Object>> due to type
            //erasure, asked
            //https://stackoverflow.com/questions/49213606/bypass-runtime-type-erasure-for-generic-map-serializer
            //for input and suggested
            //https://github.com/FasterXML/jackson-databind/issues/1966
        for(Entry<K,V> entry : entries) {
            retValue.put(entry.getKey(),
                    entry.getValue());
        }
        return retValue;
    }

    private static class Entry<K,V> {
        private K key;
        private V value;

        Entry() {
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }
}
