package richtercloud.project1.jar.entities.offer;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Objects;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import richtercloud.project1.jar.entities.Identifiable;

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id",
    scope=AbstractEntity3.class)
@XmlRootElement
public abstract class AbstractEntity3 extends Identifiable {
    private static final long serialVersionUID = 1L;
    private Set<Entity2> usedBy;
    private String name;

    public AbstractEntity3() {
    }

    public AbstractEntity3(String name) {
        this.name = name;
    }

    @XmlTransient
    public Set<Entity2> getUsedBy() {
        return usedBy;
    }

    public void setUsedBy(Set<Entity2> usedBy) {
        this.usedBy = usedBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractEntity3 other = (AbstractEntity3) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
}
