package richtercloud.project1.jar.entities;

import java.io.Serializable;

public abstract class Identifiable implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;

    protected Identifiable() {
    }

    protected Identifiable(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
